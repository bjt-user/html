#### table with borders

```
<!doctype html>
<html>
<style>
	* { font-family: "FreeMono" }
	p { font-size: 14 }
	table, th, td {
	  border: 1px solid black;
	}
</style>
<table>
	<tr>
		<th>timeline</th>
		<th>job description</th>
	</tr>
	<tr>
		<td>data</td>
		<td>data</td>
	</tr>
</table>
</html>
```

#### colored borders

```
<!doctype html>
<html>
<style>
	* { font-family: "FreeMono" }
	p { font-size: 14 }
	table, th, td {
	  border-style: solid;
	  border-width: 1px;
	  border-color: grey;
	}
	table { background:#cccccc; }
	th { background:#ffffff; }
	td { background:#ffffff; }
</style>
<table>
	<tr>
		<th>timeline</th>
		<th>job description</th>
	</tr>
	<tr>
		<td>data</td>
		<td>data</td>
	</tr>
</table>
</html>
```
