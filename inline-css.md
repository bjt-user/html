You can include `css` directly inside your `.html` file.

#### example

 ```
 <!DOCTYPE html>
<html>
<body>

<h1 style="color:blue;text-align:center;">This is a heading</h1>
<p style="color:red;">This is a paragraph.</p>
<p style="text-align:right;">This could be an address.</p>

</body>
</html> 
 ```
