#### position something at the top right of the screen with margin

If you want to have a picture at the top right of the screen but have an offset of 40 pixels from top and right side:
```
<img style="position:absolute;top:40px;right:40px" src="my_photo.jpg">
```

#### position something at the bottom

```
<p style="position: absolute;bottom: 0">this should be at the bottom</p>
```

But this doesn't seem to work if you have multiple pages.

With relative positioning this worked better, but the message was not on the bottom of the last page.\
It came after the last element on the last page.
```
<p style="position: relative;bottom: 0">this should be at the bottom</p>
```
