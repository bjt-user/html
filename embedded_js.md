#### embed javascript into html

This will print the current date in the paragraph with id "date_element":
```
<!DOCTYPE html>
<html>
<style>
	* { font-family: "FreeMono" }
</style>
<p>Hello World!</p>
<p id="date_element"></p>
<script>
	let my_date = new Date();
	document.getElementById("date_element").innerHTML = my_date;
</script>
</html>
```
It is important that the `<script>` section comes after the `<p>` element \
with the desired id.
