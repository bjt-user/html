You probably need to pick a font your browser has.


```
<p style="font-family: Arial">Hello World!</p>
```

```
<p style="font-family: Courier New">Hello World!</p>
```

If you try to use a font your browser does not know, it will use the default font \
of the browser.

#### firefox fonts

Settings -> General -> Fonts

#### change font for the entire file

You have to specify for which tags you want to change the font in the file:
```
<!doctype html>
<html>
<style>
  p { font-family: "Cantarell"; font-size: 20px }
  h4 { font-family: FreeMono; font-size: 40px }
</style>
...
```

or you can use the `*` wildcard to apply a style for every tag:
```
<style>
  * { font-family: "Cantarell" }
  p { font-size: 20px }
  h4 { font-size: 40px }
</style>
```
